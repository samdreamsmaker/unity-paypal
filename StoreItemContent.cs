﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Globalization;

public class StoreItemContent : MonoBehaviour {

	/* expose these values to user for convinience 
	 * (NOTE: these values will override any values set on the store item fields in the inspector)
	 */
	public Sprite itemImage;
	public string itemName;
	public string itemCost;
	public string itemDesc;

    [SerializeField]
	Image itemImageField;
    [SerializeField]
    Text itemNameTextField;
    [SerializeField]
    Text itemCostTextField;
    [SerializeField]
    Text itemCurCodeTextField;
    [SerializeField]
    Text itemDescTextField;

	// Use this for initialization
	void Start () {

        /*
		itemImageField = transform.FindChild("ItemImage").GetComponent<Image> ();
		itemNameTextField = transform.FindChild ("ItemName").GetComponent<Text> ();
		itemCostTextField = transform.FindChild ("ItemCost").GetComponent<Text> ();
		itemCurCodeTextField = transform.FindChild ("ItemCurCode").GetComponent<Text> ();
		itemDescTextField = transform.FindChild ("ItemDesc").GetComponent<Text> ();
		*/
		if (itemImage == null) {
			itemImage = Resources.Load <Sprite> ("ItemSprites/DefaultImage");
		}

		itemImageField.sprite = itemImage;

		if (itemName.Length > 100) {
			itemName = itemName.Substring(0,99);
		}

		itemNameTextField.text = itemName;

		itemCostTextField.text = string.Format("{0:N}", itemCost);
		itemCostTextField.text = CurrencyCodeMapper.GetCurrencySymbol (StoreProperties.INSTANCE.currencyCode) + itemCostTextField.text;

		itemCurCodeTextField.text = "(" + StoreProperties.INSTANCE.currencyCode + ")";

		itemDescTextField.text = itemDesc;

	}
		
	public void BuyItemAction() {
		Debug.Log ("Tried to buy a "  + itemName);
		StoreActions.INSTANCE.OpenPurchaseItemScreen (this);
	}
	
}
